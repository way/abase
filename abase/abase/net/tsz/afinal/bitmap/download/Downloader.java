package net.tsz.afinal.bitmap.download;

import java.io.OutputStream;

public interface Downloader  {
	
	/**
	 * 请求网络的inputStream填充outputStream
	 * @param urlString
	 * @param outputStream
	 * @return
	 */
	public boolean downloadToLocalStreamByUrl(String urlString, OutputStream outputStream);
}
